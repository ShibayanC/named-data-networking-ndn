OBJS = fshare.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c $(DEBUG)
LFLAG = -Wall $(DEBUG)

p2 : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o fshare

producer.o: producer.cpp
	$(CC) $(CFLAGS) producer.cpp

consumer.o: consumer.cpp
	$(CC) $(CFLAGS) fshare.cpp

clean:
	-rm -f p2 *.o 

tar:
	tar cfv p2.shibayan.tar wscript waf producer.cpp consumer.cpp topology.ns test.sh makefile README
