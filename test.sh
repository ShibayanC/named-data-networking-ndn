git clone https://github.com/named-data/ndn-cxx
git clone --recursive https://github.com/named-data/NFD
sudo apt-get install build-essential
sudo apt-get install libsqlite3-dev libcrypto++-dev
sudo apt-get update
sudo apt-get install build-essential
sudo apt-get install libsqlite3-dev libcrypto++-dev
sudo apt-get install libboost1.48-all-dev
sudo apt-get install doxygen graphviz python-sphinx python-pip
sudo pip install sphinxcontrib-doxylink sphinxcontrib-googleanalytics
cd ndn-cxx
./waf configure
./waf
sudo ./waf install
cd
sudo apt-get install pkg-config
sudo apt-get install libpcap-dev
sudo apt-get install doxygen graphviz python-sphinx
cd NFD
./waf configure
./waf
sudo ./waf install
cd
sudo cp /usr/local/etc/ndn/nfd.conf.sample /usr/local/etc/ndn/nfd.conf
sudo -i
nfd-start
logout
