# include <iostream>
# include <stdlib.h>
# include <string>
# include <signal.h>
# include <fstream>
# include <stdio.h>
# include <cstring>
# include <unistd.h>
# include <getopt.h>
# include <sstream>
# include <ctime>
# include <sstream>
# include <vector>
# include <ndn-cxx/face.hpp>
# include <ndn-cxx/security/key-chain.hpp>

using namespace std;

int opt, cd = 0, cp = 0;
string dwnld[10];
string publish[10];
string dld, plsh;
vector<string> wordVector;
string fileContent[100];
char myID[1024];


class Producer
{

public:
	Producer(const std::string& uri): m_prefix(uri)
	{
	}
	virtual void start()
	{
		std::cout<<"\nI am here";
		m_face.setInterestFilter(ndn::InterestFilter(m_prefix),
				bind(&Producer::onInterest, this, _1, _2),
				bind(&Producer::onRegisterSuccess, this, _1),
				bind(&Producer::onRegisterFailure, this, _1, _2));
		std::cout<<"\nPrefix is: "<<m_prefix;
		std::cout<<"\nWaiting for request";
		m_face.processEvents();
	}

	virtual void onRegisterSuccess(const ndn::Name& prefix)
	{
		std::cout << "\nSuccessfully registered " << prefix <<std::endl;
	}

	virtual void
	onRegisterFailure(const ndn::Name& prefix, const std::string& reason)
	{
		std::cout << "\nFailed to register prefix " << prefix << ": " << reason <<std:: endl;
	}

protected:
	virtual void onInterest(const ndn::InterestFilter& filter, const ndn::Interest& interest)
	{
		std::cout << "Received Interest " << interest << std::endl;
		shared_ptr<ndn::Data> response(make_shared<ndn::Data>(interest.getName()));

		/* Make the changes, open the file, take a segment and send*/
		stringstream stS(interest.toUri());
		string liS, filename, fileSegments, segMreply;
		ostringstream s;
		while(getline(stS, liS))
		{
			size_t prev = 0, pos;
			while ((pos = liS.find_first_of("?", prev)) != string::npos)
			{
				if (pos > prev)
					wordVector.push_back(liS.substr(prev, pos-prev));
				prev = pos+1;
			}
			if (prev < liS.length())
				wordVector.push_back(liS.substr(prev, string::npos));
		}
		filename = wordVector[1];
		
		stringstream ss(filename);
		while(getline(ss, liS))
		{
			size_t prev = 0, pos;
			while ((pos = liS.find_first_of("/", prev)) != string::npos)
			{
				if (pos > prev)
					wordVector.push_back(liS.substr(prev, pos-prev));
				prev = pos+1;
			}
			if (prev < liS.length())
				wordVector.push_back(liS.substr(prev, string::npos));
		}
		
		filename = wordVector[1];
		cout<<"\nHeyy"<<filename;
		
		ifstream is;
		int count = 0;
		is.open(filename.c_str(), ios::binary);
		string line;
		while (getline(is, line))
		{
			size_t len = line.length();
			char sep = ' ';
			string output;
			output.reserve(len);
			for (size_t i = 0; i < len; ++i)
			{
				output.push_back(sep);
			}
			fileContent[count] = output;
			count++;
		}

		/* sending contents over here*/
		for (int i = 0; i < count; i++)
		{
			string message = fileContent[i];
			response->setContent(reinterpret_cast<const uint8_t*>(message.c_str()),message.size());
			m_keyChain.sign(*response);
			m_face.put(*response);
		}
	}

protected:
	const ndn::Name m_prefix;
	ndn::Face m_face;
	ndn::KeyChain m_keyChain;

};


int main(int argc, char **argv)
{
	while ((opt = getopt(argc, argv, "p:d:")) != -1)
	{
		switch(opt)
		{
		case 'p':
			plsh = optarg;
			publish[cp] = plsh;
			cp++;
			break;
		default:
			cout<<"\nHuh ? Wrong option..!!";
			break;
		}
	}

	myID[1023] = '\0';
	gethostname(myID, 1023);

	cout<<"\n-------- Publish --------";
	for(int i = 0; i < cp; i++)
	{
		cout<<"\nPublish-"<<i+1<<" : "<<publish[i];
		Producer p(publish[i]);
		p.start();
	}
			
	cout<<"\n";
	return 0;
}
