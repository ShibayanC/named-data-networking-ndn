# include <iostream>
# include <stdlib.h>
# include <string>
# include <signal.h>
# include <fstream>
# include <stdio.h>
# include <cstring>
# include <unistd.h>
# include <sstream>
# include <ctime>
# include <sstream>
# include <vector>
# include <ndn-cxx/face.hpp>
# include <ndn-cxx/security/key-chain.hpp>

using namespace std;

int opt, cd = 0, cp = 0;
string dwnld[10];
string publish[10];
string dld, plsh;
vector<string> wordVector;
string fileContent[100];
char myID[1024];

class Consumer
{
public:

	Consumer(const std::string& uri)
: m_prefix(uri)
{
}

	virtual void
	start()
	{
		shared_ptr<ndn::Interest> request(make_shared<ndn::Interest>(m_prefix));
		m_face.expressInterest(*request,
				bind(&Consumer::onData, this, _1, _2),
				bind(&Consumer::onTimeout, this, _1));
		m_face.processEvents();
	}

protected:
	virtual void
	onData(const ndn::Interest& interest, const ndn::Data& data)
	{
		cout << "Received Data " << data <<endl;
		const ndn::Block& payload = data.getContent();
		cout << "payload: ";
		std::cout.write(reinterpret_cast<const char*>(payload.value()), payload.value_size());
		/* opening file with the same name as interest */

		stringstream stS(interest.toUri());
		string liS, filename, fileSegments, segMreply;
		ostringstream s;
		while(getline(stS, liS))
		{
			size_t prev = 0, pos;
			while ((pos = liS.find_first_of("/", prev)) != string::npos)
			{
				if (pos > prev)
					wordVector.push_back(liS.substr(prev, pos-prev));
				prev = pos+1;
			}
			if (prev < liS.length())
				wordVector.push_back(liS.substr(prev, string::npos));
		}
		filename = wordVector[1];

		/*  Writing to file with specific user name */

		ostringstream ctg;
		string ct;
		ofstream dfile;
		ctg <<myID<<"/"<<"csu/cs557/"<<filename;
		ct = ctg.str();								// After slash filename
		dfile.open(ct.c_str(), ios::app);
		dfile.write(reinterpret_cast<const char*>(payload.value()), payload.value_size());
		dfile.close();
		cout <<endl;
	}

	virtual void
	onTimeout(const ndn::Interest& interest)
	{
		std::cout << "Interest timed out " << interest << std::endl;
		exit(0);
	}

protected:
	const ndn::Name m_prefix;
	ndn::Face m_face;
};

int main(int argc, char **argv)
{
	while ((opt = getopt(argc, argv, "p:d:")) != -1)
	{
		switch(opt)
		{
		case 'd':
			dld = optarg;
			dwnld[cd] = dld;
			cd++;
			break;
		default:
			std::cout<<"\nHuh ? Wrong option..!!";
			break;
		}
	}
	cout<<"\n------- Download ----------";
	for(int i = 0; i < cd; i++)
	{
		std::cout<<"\nDownload-"<<i+1<<" : "<<dwnld[i];
		Consumer c(dwnld[i]);
		c.start();
	}
	return 0;
}
